import React from "react";
import "../App.css";

export interface Props {
    content:string;
    clickEvent:()=>void;
}
 
const StopwatchButton: React.FC<Props> = ({content,clickEvent}) => {
    return (
        <div className="control">
        <button onClick={clickEvent}>{content}</button>
        </div>
      );
}
 
export default StopwatchButton;