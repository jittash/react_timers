import React from "react";
import "../App.css";

export interface Props {
    time:string
}
 
const StopwatchTimer: React.FC<Props> = ({time}) => {
    return ( 
    <div className="watch-timer">
    {time}
    </div> 
    );
}
 
export default StopwatchTimer;