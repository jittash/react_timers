import React from 'react';
import './App.css';
import Stopwatch from './components/Stopwatch';
import Countdown from './components/Countdown';

function App() {
  return (
    <div className="App">
      <Countdown startTime={259460} />
      <br />
      <br />
      <Stopwatch />

    </div>
  );
}

export default App;
